package com.inedo.demo;

import org.joda.time.LocalTime;

public class HelloWorld {
    public static void main(String[] args) {
      LocalTime currentTime = new LocalTime();
		  System.out.println("The current local time is: " + currentTime);

        Greeter greeter = new Greeter();
        System.out.println(greeter.sayHello());
        // Use this after publishing com.inedo.demo.stringpadderlib (https://gitlab.com/inedo-samples/javastringpadderlib) to ProGet
        // StringPadder padder = StringPadderFactory.createStringPadder();
        // System.out.println(padder.padLeft(greeter.sayHello(), 30));
    }
}
